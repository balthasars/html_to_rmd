As suggested above by G. Grothendieck, start by running

    pandoc path/to/your/document -o name_of_your_document.md

in your shell, make sure you're in the target directory where you'd like your `.md` file.

Perform the following Regex search-and-replaces in the text editor of your choice.
Please note that I've only written this just now in Atom on two sample files;
you may have to adjust the order or the replace actions.

 1. ` {.r}`

 1.1. search: `\s{\.r[^}]*}`

 1.2. replace: `{r}`

 2. `<div>`

 2.1. search: `<div[^>]*>`

 2.2. replace: ` `

 3. `</div>`

 3.1 search: `</div[^>]*>`

 3.2. replace: ` `

 4. `===================` of any length

 4.1. search: `^=.*`

 4.2. replace: ` `

 5. `{.citation}`

 5.1. search: `{.c[^}]*}`

 5.2. replace: ` `

 6. Elements such as `{#difference-in-differences .title .toc-ignore}`, starting with `#`

 6.1. search: `{\#[^}]*}`

 6.2. replace: ` `

 7. Broken image references like `![](data:image/png;base64,iVB0dDbQ2TVjShtLp....`{width="672}`:

 7.1. search: `^\!\[\]\(.*\}$`

 7.2. replace: ` `

 8. Wrap rendered results into backticks ` ``` `

 8.1. search: `\`{3}\n{2}(\s{4}\#{2}.*\n){1,}$`

 8.2 replace: ` \n```\n$&``` `

 9. unnecessary line white space due to line breaks

 9.1. search: ` (\n{3,}) `

 9.2. replace: `\n\n`

# prelim
10. remove `------`

10.1. search: `^-{2,}`

10.2. replace: ` `

11. Rename your file to `.Rmd`.

Now you can more easily work with your file in RStudio.

What this can't do:
- reconvert formulae to LateX/Markdown format
- reconvert former H1-titles that are now underlined like `------------------`
- spaces between \`{3}\n\`{3}
- extra line break after backticks marking code chunk end \`{3}\n^[0-9A-Za-z ]*[0-9A-Za-z]+[ ]*$
- extra line break before code if output is only one line

# Only get code chunks:

maybe like this: ^\`{3}\{r\}\n(.*?\n){1,}\`\n{3}
